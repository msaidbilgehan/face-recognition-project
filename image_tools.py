from tools import stdo, get_time    # For properly print out
from inspect import currentframe, getframeinfo
from PIL import Image
import cv2
import skimage as ski
import numpy as np
import os   # For remove or check files
import errno    # https://stackoverflow.com/questions/36077266/how-do-i-raise-a-filenotfounderror-properly
# import time # Delay for camera module
import random
from time import sleep

# https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/
def take_images(option):
    if option == ("cam" or "pc"):
        """
            Not necessary but may help with PC applications
        """
        # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html

        cap = cv2.VideoCapture(0)
        # Capture frame-by-frame
        success, frame = cap.read()
        if success:
            # Save the resulting frame to the shared area
            #shared.srcImage["image"] = frame

            # When everything done, release the capture
            cap.release()
        else:
            stdo( 3, "Camera connection failed.",  getframeinfo(currentframe()) )
            exit(-1)

    if option == "piCam":
        import pdb; pdb.set_trace() # Pi Camera DEBUG
        # For take pictures from Raspberry Pi Camera
        from picamera.array import PiRGBArray
        from picamera import PiCamera

        # initialize the camera and grab a reference to the raw camera capture
        camera = PiCamera()
        rawCapture = PiRGBArray(camera)

        # allow the camera to warmup
        # time.sleep(0.1)

        # grab an image from the camera
        camera.capture(rawCapture, format="bgr")
        #shared.srcImage["image"] = rawCapture.array

    return 0

"""
def get_images(path):
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    i = 1
    for file in onlyfiles:
        # "{}".format(file)
        fileData = {
            "image": open_images(file, "cv2-grayscale"),
            "type": "<class 'numpy.ndarray'>",
            "name": "{}".format( str(file) ),
            "path": "{}".format( path ),
            "size": 0,
            "width": 0,
            "heigth": 0,
            "channel": 0,
            "labels": 0,
            "OCR": ""
        }

        fileData = {
            "size": fileData["image"].size,
        }

        fileData["width"], fileData["heigth"], fileData["channel"] = fileData["image"].shape

        #shared.tempImageList[i, fileData ]
        i += 1
"""

def save_image(img, path=""):
    try:
        if path == "":  # If path is empty, then we refer to temp file save protocol
            filename = get_temp_name()
            path = "tempFiles/{}".format( filename )

        dirList = path.split('/')
        filename = dirList[-1]
        dirList = dirList[:-1]

        if dirList != []:
            directory = ""
            for dir in dirList:
                directory += dir + "/"

            if not os.path.exists( directory ):
                os.makedirs(directory, exist_ok = True, mode=0o777)

            if not os.path.exists( directory ):
                raise IOError
            else:
                stdo(1, "'{}' Directory directory created and image saved as '{}'.".format(directory, filename) )   # filename = str(path.split('/')[-1])
                cv2.imwrite(path, img)

        stdo(1, "Image will be saved as '{}'".format(filename) )
        cv2.imwrite(path, img)
        return path

    except IOError as error:
        stdo(3, "Directory creation failed, please raport this issue to developer -> {}".format( error.__str__(), getframeinfo(currentframe() ) ) )
        return -1

    except Exception as error:
        stdo(3, "An error occured while saveImage function runtime -> " + error.__str__(), getframeinfo(currentframe() ) )
        return -1
    return 0


def delete_images(path):
    try:
        if os.path.exists(path):
            os.remove(path)
        else:
            raise Exception(errno.ENOENT, os.strerror(errno.ENOENT), path)
    except Exception as error:
        stdo(3, "An error occured while deleteImage function runtime -> " + error.__str__(),  getframeinfo(currentframe() ) )
        return -1
    return 0


def show_images(img, title = "Title", window = False):
    stdo(1, "Image will be shown now with title '{}'.".format(title))

    if window:
        cv2.namedWindow(title, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(title, 600, 600)
        try:
            cv2.imshow( title, img )
            cv2.waitKey()
            cv2.destroyAllWindows()
        except Exception as error:
            stdo(3, "An error occured while working in showImage function -> " + error.__str__(),  getframeinfo(currentframe() ) )
            stdo(1, "--- --- ---")

        """ # For skimage
        plt.imshow(img, cmap='gray', interpolation='nearest')
        plt.show()
        """
    return 0


def rgb2gray(img, verbose = False):
    """
        rgb2gray takes 2 parameter;
            img is for stored image variable to convertion of grayscale. This parameter is necessary.
            verbose for more detail in debug on terminal. This parameter is optional.
    """
    try:
        if len(img.shape) > 2:
            if img.shape[-1] != 1:
                if verbose:
                    stdo(1, "RGB Image converted to grayscale.")
                return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            else:
                return img
        else:
            if verbose:
                stdo(1, "Image is already grayscale. Returning given image...")
            return img
    except Exception as error:
        stdo(3, "An error occured while working in rgb2gray function -> " + error.__str__(),  getframeinfo(currentframe() ) )
        stdo(1, "--- --- ---")
    exit()


def compare2images(img, img2, title = "Title"):
    """
        compare2Images takes 3 parameter;
            img and img2 for comparing both at the same window. This parameter is necessary.
            title for showing them in a spesific title. This parameter is optional.
    """
    stdo(1, "Images will be shown now with title '{}'.".format(title) )
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)

    try:
        cv2.imshow( title, np.hstack([img, img2]) )
        cv2.waitKey()
        cv2.destroyAllWindows()
    except Exception as error:
        stdo(3, "An error occured while working in compare2Images function -> " + error.__str__(),  getframeinfo(currentframe() ) )
        stdo(1, "--- --- ---")
    return 0


def info_images(img = None, option = ""):
    # option for skimage and numpy image desicion (Still at development state)
    try:
        # https://www.programiz.com/python-programming/methods/built-in/isinstance
        output = """Image
                    |- Width: {0}
                    |- Height: {1}
                    |- Size: {2}
                    '- Type/Class: {3}"""
        stdo(1, output.format(
            str(img.shape[0]),
            str(img.shape[1]),
            str(img.size),
            str(type(img))
        ) )
    except Exception as error:
        stdo(3, "An error occured while working in infoImage function -> " + error.__str__(),  getframeinfo(currentframe() ) )
        stdo(1, "--- --- ---")
    return 0


def get_temp_name(seed = get_time(level=0)):
    sleep(1)
    random.seed(seed)
    name = get_time(2)
    randomInt = str(random.randint(1000, 9999))

    if name[-1] == "_":
        name = name[:-1] + "0_" + randomInt
    else:
        name = name + "_" + randomInt

    return "temp_{}.{}".format(name, "png")


def infoVideo(video = 0, option = ""):
    # option for skimage and numpy image desicion

    try:
        # https://www.programiz.com/python-programming/methods/built-in/isinstance
        output = """Video
                    |- Width: {0}
                    |- Height: {1}
                    |- FPS: {2}
                    |- Size: {3}
                    '- Type/Class: {4}"""
        if not isinstance(video, int):
            height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))  # always 0 in Linux python3
            width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))  # always 0 in Linux python3
            fps = video.get(cv2.CAP_PROP_FPS)
            stdo(1, output.format(
                str(width),
                str(height),
                str(fps),
                str(width*height),
                str(type(video))
            ) )
        else:
            raise Exception
    except Exception as error:
        stdo(3, "An error occured while working in infoVideo function -> " + error.__str__(),  getframeinfo(currentframe() ) )
        stdo(1, "--- --- ---")
    return 0
