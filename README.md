# Image Processing and Face Recognition

Written with Python

Tested with Python 3.5.3, and DeepinOS 15.11 Stable

# Project Details

Project started for Image Processing

Necessary Dependencies for Python 3.5.3

Usage:

    - After running "run.py" file, it will show a menu which will lead necessary options.

Example Usage:

    - python3 run.py

# Predefined Options

Images Directories;

    - in train directory, you need to give person name to folder, which should have face images. Do not give more than 1 face in a images.
      train/
        |- Said/
          |--1.jpg
          |--2.jpg
          '--3.jpg
        |- John/
          |--1.jpg
          |--2.jpg
          '--3.jpg
        '- Berk/
          |--1.jpg
          |--2.jpg
          '--3.jpg

    - In test directory, there should be no folder, only test image(s).
      test/
        |-test1.jpg
        |-test2.jpg
        '-test3.jpg

# Different Filtering and Manipulation Options

Program has a tuned image manipulation system. Its API parameters in "run.py" file. Also, after processing, processed images will be stored at "preprocessed" folder and at every run, folder will be cleared.

##Image Open Parameters Configurations;

  - option = M_Engine.open_image_options.cv2_rgb
  - resize = None

  For resizing with ratio lock;

  - resize = (1024, None)

##Manipulation Parameters Configurations;
  For manipulation of all images in cache, give None. Otherwise it will look for specific person image

  - image_name = None

  Algorithms for manipulations

  - algorithms = [M_Engine.manupilate_options.opening, M_Engine.manupilate_options.closing]

  Custom kernel, which will be deactivate custom kernel parameters

  - kernel = None

  Predefined kernel shape

  - kernel_shape = M_Engine.kernel_shape.rectangular

  Size of predefined kernel

  - kernel_size = (5, 5)

  How much time it will be processed again

  - render = 1

  After parameters definitions, call the manipulation engine's manipulate options;

  - M_Engine.manupilate(image_name, algorithms, kernel, kernel_shape, kernel_size, render)
