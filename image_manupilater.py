# https://stackoverflow.com/questions/44650888/resize-an-image-without-distortion-opencv
import os
import cv2
import numpy as np
from enum import Enum
from matplotlib import pyplot as plt
from PIL import Image, ImageFilter
from inspect import currentframe, getframeinfo
from tools import stdo, list_files
from image_tools import save_image


class Manupilation_Engine(object):

    class open_image_options(Enum):
        cv2_rgb = cv2.IMREAD_COLOR
        cv2_gray = cv2.IMREAD_GRAYSCALE


    class manupilate_options(Enum):
        closing = cv2.MORPH_CLOSE
        opening = cv2.MORPH_OPEN


    class kernel_shape(Enum):
        rectangular = cv2.MORPH_RECT
        elliptical = cv2.MORPH_ELLIPSE
        cross = cv2.MORPH_CROSS


    image_block = {
        "name": None,
        "type": None,
        "images": list(),
    }
    image_container = {
        "property": None,
        "location": None,
        "image": None,
        "manupilated_image": None
    }


    def __init__(self):
        self.image_list = list()
        self.image_counter = 0
        self.image_block_counter = 0
        self.output = ""


    def _open_image(self, image_loc, option = open_image_options.cv2_gray, resize = None):
        output = "'{}' Image opened with '{}' option.".format( image_loc.split("/")[-1], option )

        try:
            if resize is not None:      # Read image as colorfull (RGB) or grayscale & Resize it
                stdo(1, output)
                return self.image_resize( cv2.imread(image_loc, option.value), resize[0], resize[1] )
            else:
                stdo(1, output)
                return cv2.imread(image_loc, option.value)    # Read image as colorfull (RGB) or grayscale

            stdo(3, "Option Not Found! -> " + option)
            stdo(1, "Image open process will use default (RGB - 800x800) parameters...")
            return self.image_resize( cv2.imread(image_loc, open_image_options.cv2_rgb), 800, 800 )

        except IOError as ioe:
            stdo(3, "Image Not Found! -> " + ioe.__str__(),  getframeinfo( currentframe() ) )
            stdo(1, "Image open process skipped.")
            return -1

        except FileNotFoundError as fnfe:
            stdo(3, "File Not Found! -> " + fnfe.__str__(),  getframeinfo( currentframe() ) )
            stdo(1, "Image open process skipped.")
            return -1

        except Exception as error:
            stdo(3, "An error occured while working in openImage function -> " + error.__str__(),  getframeinfo( currentframe() ))
            stdo(1, "Image open process skipped.")
            return -1


    def new_image(self, image_loc, option = open_image_options.cv2_gray, resize = None):
        if os.path.isdir(image_loc):
            image_files = list_files(path = image_loc, name = "*", extensions = ["png", "jpg", "jpeg"], recursive = True, verbose = True)
        else:
            image_files = [image_loc]


        if (image_files is not None) and len(image_files) != 0:

            name_file_list_dict = dict()
            for path in image_files:
                current_name = path.split('/')[-2]

                if current_name not in name_file_list_dict:
                    name_file_list_dict[ current_name ] = list()

                name_file_list_dict[ current_name ].append(path)


            for name, path_list in name_file_list_dict.items():

                image_block = self.image_block.copy()
                image_block["name"] = name
                image_block["type"] = option

                for path in path_list:
                    image_container = self.image_container.copy()
                    image_container["property"] = path.split("/")[-1]
                    image_container["location"] = path
                    image_container["image"] = self._open_image(path, option, resize)

                    image_block["images"].append(image_container)
                    self.image_counter += 1


                self.image_list.append(image_block)
                self.image_block_counter += 1

        else:

            image_block = self.image_block.copy()
            image_block["name"] = image_loc.split("/")[-1]
            image_block["type"] = option

            image_container = self.image_container.copy()
            image_container["property"] = image_block["name"]
            image_container["location"] = image_loc
            image_container["image"] = self._open_image(image_loc, option)

            self.image_list.append(temp_image_container)
            self.image_counter += 1
            self.image_block_counter += 1


    # https://stackoverflow.com/questions/44650888/resize-an-image-without-distortion-opencv
    @staticmethod
    def image_resize(image, width = None, height = None, inter = cv2.INTER_CUBIC):
        # initialize the dimensions of the image to be resized and
        # grab the image size
        dim = None
        (h, w) = image.shape[:2]

        # if both the width and height are None, then return the
        # original image
        if width is None and height is None:
            return image

        # check to see if the width is None
        if width is None:
            # calculate the ratio of the height and construct the
            # dimensions
            r = height / float(h)
            dim = (int(w * r), height)

        # otherwise, the height is None
        else:
            # calculate the ratio of the width and construct the
            # dimensions
            r = width / float(w)
            dim = (width, int(h * r))

        # resize the image
        resized = cv2.resize(image, dim, interpolation = inter)

        # return the resized image
        return resized


    @staticmethod
    def _manupilate(image, algorithm = manupilate_options.opening, kernel = None, kernel_shape = kernel_shape.rectangular, kernel_size = (3, 3), render = 1):

        if kernel is None:
            kernel = cv2.getStructuringElement(kernel_shape.value, kernel_size)

        manupilated_image = image
        for i in range(render):
            manupilated_image = cv2.morphologyEx(manupilated_image, cv2.MORPH_OPEN, kernel)

        return manupilated_image


    def manupilate(self, image_name = None, algorithms = [manupilate_options.opening], kernel = None, kernel_shape = kernel_shape.rectangular, kernel_size = (3, 3), render = 1 ):

        counter = 0
        if image_name is not None:
            for image_block in self.image_list:
                if image_block["name"] == image_name:
                    for image_container in image_block["images"]:
                        image_container["manupilated_image"] = image_container["image"]
                        for algorithm in algorithms:
                            for r in range(render):
                                image_container["manupilated_image"] = self._manupilate(image_container["manupilated_image"], algorithm, kernel, kernel_shape, kernel_size)

                        counter += 1

                    return counter

        else:
            for image_block in self.image_list:
                for algorithm in algorithms:
                    for image_container in image_block["images"]:
                        image_container["manupilated_image"] = image_container["image"]
                        for algorithm in algorithms:
                            for r in range(render):
                                image_container["manupilated_image"] = self._manupilate(image_container["manupilated_image"], algorithm, kernel, kernel_shape, kernel_size)
                        counter += 1

            return counter

        return None


    def save_cached_image_list(self, save_root_path = "processed_images/", is_train = True):

        if save_root_path[-1] != "/":
            save_root_path = save_root_path + "/"

        root_image_path_list = list()
        root_image_path_list.append(save_root_path)
        for image_block in self.image_list:
            face_name = image_block["name"] + "/"

            for image_container in image_block["images"]:
                image_file_name = image_container["property"]
                if is_train:
                    current_save_path = save_root_path + face_name + image_file_name
                else:
                    current_save_path = save_root_path + image_file_name

                root_image_path_list.append(image_container["location"])
                save_image(image_container["manupilated_image"], current_save_path)
        return root_image_path_list


    def get_image_list(self):
        return self.image_list


    def get_source_image_list(self):
        image_list = list()

        for image_block in self.image_list:
            for image_container in image_block["images"]:
                image_list.append(image_container["image"])
        return image_list


    def get_manupilated_image_list(self):
        image_list = list()

        for image_block in self.image_list:
            for image_container in image_block["images"]:
                image_list.append(image_container["manupilated_image"])
        return image_list


    def plot(self, image_name_list, figure_size = (7, 7), subplot_size_matrix = [2, 2], cmap = "gray", aspect = "auto"):
        plt.figure( figsize = figure_size )

        subplot_size = subplot_size_matrix[0] * 100 + subplot_size_matrix[1] * 10
        image_counter = 0

        for image_container in self.image_list:
            #for image_name in image_name_list:
            if image_container["name"] in image_name_list:
                image_counter += 1

                subplot_size += 1
                plt.subplot(subplot_size), plt.imshow(image_container["image"], cmap = cmap, aspect = aspect), plt.title(image_container["name"])

                subplot_size += 1
                if image_container["manupilated_image"] is not None:
                    plt.subplot(subplot_size), plt.imshow(image_container["manupilated_image"], cmap = cmap, aspect = aspect), plt.title(image_container["name"])

                plt.xticks([]), plt.yticks([])

        if image_counter > 0:
            plt.show()
            return image_counter
        else:
            return None
