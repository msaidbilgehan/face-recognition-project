# https://github.com/CITGuru/PyInquirer
# https://github.com/CITGuru/PyInquirer/blob/master/examples/pizza.py

from __future__ import print_function, unicode_literals

# import regex

from PyInquirer import prompt
# from PyInquirer import style_from_dict, Token
# from PyInquirer import Validator, ValidationError

from examples import custom_style_3
from pprint import pprint
from tools import stdo

from enum import Enum


class main_decision(Enum):
    exit = -1
    predict = 0
    train_old_model = 1
    train_new_model = 2


def main():

    print('\n\t --- Face Recognition with Tuned Image Processing --- \n')

    questions = [
        {
            'type': 'list',
            'name': 'main_decision',
            'message': 'What do you need?',
            'choices': [
                {
                    'name': 'Predict Name with Input',
                    'value': main_decision.predict
                },
                {
                    'name': 'Train the NEW model',
                    'value': main_decision.train_new_model
                },
                {
                    'name': 'Exit',
                    'value': main_decision.exit
                }
            ]
        }
    ]

    answers = prompt(questions, style = custom_style_3)
    if answers["main_decision"] is main_decision.exit:
        exit()

    elif answers["main_decision"] is main_decision.predict:
        questions = [
            {
                'type': 'input',
                'name': 'model_path',
                'message': 'Face Model Path to Prediction of Image:',
                'default': 'trained_knn_model.clf'
            },
            {
                'type': 'input',
                'name': 'image_path',
                'message': 'Path of image dir (should be folder path) which will be predicted:',
                'default': 'faces/test'
            }
        ]
        """
    elif answers["main_decision"] is main_decision.train_old_model:
        questions = [
            {
                'type': 'input',
                'name': 'model_path',
                'message': 'Old Face Model Path to Use:',
                'default': 'trained_knn_model.clf'
            },
            {
                'type': 'input',
                'name': 'image_path',
                'message': 'Path of image(s) dir (should be folder path) which will be trained with:',
                'default': 'faces/train'
            }
        ]
        """
    else:
        questions = [
            {
                'type': 'input',
                'name': 'model_path',
                'message': 'New Face Model Path to Save:',
                'default': 'trained_knn_model.clf'
            },
            {
                'type': 'input',
                'name': 'image_path',
                'message': 'Path of image(s) dir (should be folder path) which will be trained with:',
                'default': 'faces/train/'
            }
        ]

    answers.update( prompt(questions, style = custom_style_3) )

    return answers


def print_answers(answers_dict):

    stdo(1, 'Answers:')
    pprint(answers_dict)
