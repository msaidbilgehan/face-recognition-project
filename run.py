# IMPORTS
from tools import stdo, clear_dir    # For properly print out
import argument_parser as ap
import face_recognition_api as fra
from image_manupilater import Manupilation_Engine
# from image_tools import save_image
import cli
# from os import path

# GLOBALS
ARGS = None
M_Engine = None


def init_arguments():
    global ARGS
    ARGS = ap.get_arguments()
    ap.output_argument_info(ARGS)


def init_cli():
    global ARGS
    ARGS = cli.main()


def init_process():
    global M_Engine

    M_Engine = Manupilation_Engine()


def optimum_preprocessing_images(is_train, image_dir = "faces/", new_dir = "preprocessed/", verbose = True):
    if not isinstance(is_train, bool):
        stdo(2, "Train parameter should be bool, such as 'True' or 'False'")
        return -1
    if new_dir[-1] != "/":
        new_dir += "/"
    if is_train:
        new_dir += "train/"
    else:
        new_dir += "test/"

    clear_dir(new_dir)

    # Image Open Parameters Config
    option = M_Engine.open_image_options.cv2_rgb
    resize = None
    # resize = (1024, None)

    # Manupilation Parameters Config
    image_name = None   # For manupilation of all images in cache
    algorithms = [M_Engine.manupilate_options.opening, M_Engine.manupilate_options.closing]
    kernel = None
    kernel_shape = M_Engine.kernel_shape.rectangular
    kernel_size = (5, 5)
    render = 1

    if verbose:
        stdo(1, "\n\t- option: {}\n\t- resize: {}\n\t- image_name: {}\n\t- algorithms: {}\n\t- kernel: {}\n\t- kernel_shape: {}\n\t- kernel_size: {}\n\t- render: {}".format(option, resize, image_name, algorithms, kernel, kernel_shape, kernel_size, render))

    M_Engine.new_image(image_dir, option, resize)
    M_Engine.manupilate(image_name, algorithms, kernel, kernel_shape, kernel_size, render)
    M_Engine.save_cached_image_list(new_dir, is_train)
    return new_dir


# FUNCTIONS
def main():
    global ARGS, M_Engine

    # Initilize arguments and manupilation engine
    init_process()

    while(True):
        init_cli()
        cli.print_answers(ARGS)

        # Take actions according to the given decisions
        if ARGS["main_decision"] is cli.main_decision.predict:
            """
            if path.isdir(ARGS["image_path"]):
                processed_images_dir += ARGS["image_path"].split("/")[-1]
            else:
                processed_images_dir += ARGS["image_path"].split("/")[-2]
            """

            processed_images_dir = optimum_preprocessing_images(False, ARGS["image_path"] )
            predictions = fra.predict(test_image_dir = processed_images_dir, model_path = ARGS["model_path"], distance_threshold = 0.6, verbose = True, display = True)

        elif ARGS["main_decision"] is cli.main_decision.train_new_model:
            processed_images_dir = optimum_preprocessing_images(True, ARGS["image_path"] )
            fra.train_model(train_image_dir = processed_images_dir, model_save_path = ARGS["model_path"], n_neighbors = 2)

            """
        elif ARGS["main_decision"] is cli.main_decision.train_old_model:
            stdo(3, "This part Still at development state...")
            stdo(1, "Redirecting to menu.")
            # exit()
            """
            """
            M_Engine.new_image(processed_images_dir)
            fra.predict(test_image_dir = processed_images_dir, model_path = ARGS["model_path"], distance_threshold = 0.6, verbose = True, display = False)
            """

        """
        image_names = ("image_1", "image_2")
        figure_size = (10, 10)
        M_Engine.plot(image_name_list = image_names, figure_size = figure_size, subplot_size_matrix = [2, 2], cmap = "gray")

        # save_image_list(M_Engine.get_manupilated_image_list())
        """

# BODY
main()
