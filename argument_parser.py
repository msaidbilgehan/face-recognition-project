# IMPORTS
from tools import stdo    # For properly print out
from inspect import currentframe, getframeinfo
import argparse

"""
    Example Usage:
        python3 run.py
"""

# GLOBALS
ARGS_CACHE = {}
ARGS_OUTPUT = ""
args = None

# FUNCTIONS
# https://stackoverflow.com/questions/15753701/how-can-i-pass-a-list-as-a-command-line-argument-with-argparse
def get_arguments(want_return = True):
    global args

    aP = argparse.ArgumentParser()  # Parse arguments


    aP.add_argument("-i", "--image", default = "faces/",
                    help = "NOT NECCESSARY - specifiy image location, default option which is local image folder faces/")

    aP.add_argument("-m", "--manipulation", default = "opening",
                    help = "Manipulations which need to applying to image such as opening - closing algorithms.")     # Image path argument

    # This is the correct way to handle accepting multiple arguments.
    # '+' == 1 or more.
    # '*' == 0 or more.
    # '?' == 0 or 1.
    # An int is an explicit number of arguments to accept.
    """
    aP.add_argument("-k", "--kernel", default = "rectangular",
                    help="NOT NECCESSARY - kernel matrix for openning - closing algorithms (such as 'dimond - default')")   # Processing type
    """

    if want_return:
        if args is None:
            args = vars(aP.parse_args())
        return args
    else:
        args = vars(aP.parse_args())
        return None


def output_argument_info(arguments = None, only_cache = False):
    # https://docs.python.org/3/tutorial/datastructures.html
    # https://stackoverflow.com/questions/9453820/alternative-to-python-string-item-assignment

    try:
        global ARGS_CACHE, ARGS_OUTPUT
        # imageName = args["image"].split("/")[-1]

        if arguments is None:
            arguments = ARGS_CACHE

        arguments_output = "Given Arguments:\n"
        for arg in list(arguments):
            arguments_output += "\t\t    |- {} : {}\n".format(arg, args[arg])

        list_of_args = list(arguments_output)
        list_of_args[ arguments_output.rfind("|") ] = "'"

        for char in list_of_args:
            ARGS_OUTPUT += char

        if not only_cache:
            stdo(1, ARGS_OUTPUT)

        return None

    except Exception as error:
        stdo(3, "An error occured while shareArgs function runtime in main run.py -> " + error.__str__(),  getframeinfo(currentframe() ) )
        return -1


def argInfo(args_output = None):
    global ARGS_OUTPUT
    if args_output is None:
        stdo(1, ARGS_OUTPUT)
    else:
        stdo(1, args_output)

    return None
